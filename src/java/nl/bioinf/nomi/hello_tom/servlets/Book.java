/*
 * Copyright (c) 2015 Michiel Noback [m.a.noback@pl.hanze.nl]
 * All rights reserved.
 */
package nl.bioinf.nomi.hello_tom.servlets;
/**
 *
 * @author Michiel Noback [m.a.noback@pl.hanze.nl]
 * @version 0.0.1
 */
public class Book {
    /**
     * the book title.
     */
    private String title;
    /**
     * the book author.
     */
    private String author;
    /**
     * the number of pages
     */
    private int pages;

    /**
     * default constructor.
     */
    public Book() {}

    /**
     * utility constructor.
     * @param title
     * @param author
     * @param pages 
     */
    public Book(String title, String author, int pages) {
        this.title = title;
        this.author = author;
        this.pages = pages;
    }

    /**
     *
     * @return
     */
    public String getTitle() {
        return title;
    }

    /**
     *
     * @param title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     *
     * @return
     */
    public String getAuthor() {
        return author;
    }

    /**
     *
     * @param author
     */
    public void setAuthor(String author) {
        this.author = author;
    }

    /**
     *
     * @return
     */
    public int getPages() {
        return pages;
    }

    /**
     *
     * @param pages
     */
    public void setPages(int pages) {
        this.pages = pages;
    }

}