/*
 * Copyright (c) 2015 Michiel Noback [m.a.noback@pl.hanze.nl]
 * All rights reserved.
 */
package nl.bioinf.nomi.hello_tom.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Michiel Noback [m.a.noback@pl.hanze.nl]
 */
@WebServlet(name = "LoginServlet", urlPatterns = {"/login.do"})
public class LoginServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        String user = request.getParameter("username");
        String pass = request.getParameter("password");

        HttpSession session = request.getSession();
        session.setMaxInactiveInterval(10);
        //check if there is a logged in user
        User sessionUser = (User) session.getAttribute("user");
        if (sessionUser == null) {
            //there is no logged in user
            //login attempt
            User userObj = checkCredentials(user, pass);
            if (userObj == null) {
                //login fails
                String errorMessage = "You provided incorrect credentials; please try again!";
                request.setAttribute("errorMessage", errorMessage);
                RequestDispatcher view = request.getRequestDispatcher("jsp/login.jsp");
                view.forward(request, response);
            } else {
                //login succeeds
                session.setAttribute("user", userObj);
                request.setAttribute("books", getBooks());
                RequestDispatcher view = request.getRequestDispatcher("jsp/content.jsp");
                view.forward(request, response);
            }
        } else {
            //user already logged in; forward to content page
            request.setAttribute("books", getBooks());
            RequestDispatcher view = request.getRequestDispatcher("jsp/content.jsp");
            view.forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher view = request.getRequestDispatcher("login.jsp");
        view.forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    /**
     * provides a collection of Books.
     * @return books
     */
    private List<Book> getBooks() {
        List<Book> books = new ArrayList<>();
        books.add(new Book("the anarchists' manual", "John Doe", 666));
        books.add(new Book("How to build a bomb", "Jane Doe", 65));
        books.add(new Book("Alice in Wonderland", "Carroll", 98));
        return books;
    }
    
    /**
     * methods checks credentials for login attempt.
     * @param user the username
     * @param pass the password
     * @return user if credentials OK, else null
     */
    private User checkCredentials(final String user, final String pass) {
        //two hardcoded users registered
        if (user.equals("Henk") && pass.equals("henkje")) {
            User u = new User();
            u.setName(user);
            u.setEmail(user + "@example.org");
            return u;
        } else if (user.equals("Michiel") && pass.equals("vogel")) {
            User u = new User();
            u.setName(user);
            u.setEmail(user + "@example.org");
            return u;
        } else {
            return null;
        }
    }
}
