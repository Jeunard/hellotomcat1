/*
 * Copyright (c) 2015 Michiel Noback [m.a.noback@pl.hanze.nl]
 * All rights reserved.
 */
package nl.bioinf.nomi.hello_tom.servlets;
/**
 *
 * @author Michiel Noback [m.a.noback@pl.hanze.nl]
 * @version 0.0.1
 */
public class User {
    private String name;
    private String email;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    public String getUserMessage() {
        return "Hello " + this.name + " [" + this.email + "]";
    } 
    
}